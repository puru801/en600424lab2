
def getPrivateKeyForAddr(addr):
        with open("Certs/privatekey") as f:
            c = f.read()
        return c

def getCertsForAddr(addr):
    c=[]
    with open("Certs/"+str(addr)) as f:
        c.append(f.read())
    with open("Certs/pkulkarni_signed.cert") as f:
        c.append(f.read())

    return c

def getRootCert():
    with open("Certs/20164_signed.cert") as f:
        c = f.read()

    return c
